#!usr/bin/env python3

# this is a set of classes for reading potentiometers
# they will run through the gpio pins

import time
import os
import RPi.GPIO as gpio
import db_connect
import threading
import multiprocessing as mp

class potentiometers:
	def __init__(self):
		# setting how we refer to the gpio pins
		#gpio.setmode(gpio.BCM)
		# I believe scotty chose these pins
		self.SPICLK = 18
		self.SPIMISO = 23
		self.SPIMOSI = 24
		self.SPICS = 25
		# set up the gpio pins
		gpio.setup(self.SPIMISO, gpio.IN)
		gpio.setup(self.SPIMOSI, gpio.OUT)
		gpio.setup(self.SPICLK, gpio.OUT)
		gpio.setup(self.SPICS, gpio.OUT)
		# a for-loop to set the values of an array 
		# for the channels of the potentiometers
		pot_adc = []
		for i in range(0,8):
			pot_adc.append(i)
		# the amount the pot needs to be opened 
		# before the program registers it
		self.tolerance = 650
		# the bool array that will be passed to show which pots are open
		self.openBins = [False, False, False, False, False, False, False, False]
	# runs a loop to check all of the potentiometers
	# RETURNS:	hopefully a command to the main thread to do something
	def checkPotentiometers(self, queue):
		self.queue = queue
		while True:
			for i in range(0,8):
				opened = self.checkPot(i)
				if opened:
					self.openBins[i] = True
				else:
					self.openBins[i] = False
			if self.queue.empty():
				self.queue.put(self.openBins)
			time.sleep(0.2)
	# checks a specific pot to see if it has opened
	# ARG: 		adc = channel of pot
	# RETURNS:	True if opened to a certain degree
	#			False if not
	def checkPot(self, adc):
		# start with the assumption that the bin is closed
		# if bin has been opened and closed, this will still catch the box closing
		trim_pot_changed = False
		# read the specific pin set by pot_adc
		trim_pot = self.readADC(adc, self.SPICLK, self.SPIMOSI, self.SPIMISO, self.SPICS)
		# what's the change
		pot_adjust = abs(trim_pot)
		# checks if the change is past the tolerance
		if (pot_adjust > self.tolerance):
			trim_pot_changed = True
		return trim_pot_changed
	# reads the data from the channel passed along
	# as a parameter (0-7)
	def readADC(self, adcnum, clockpin, mosipin, misopin, cspin):
		# makes sure the adcnum is in range
		if ((adcnum > 7) or (adcnum < 0)):
			return -1
		gpio.output(cspin, True)
		gpio.output(clockpin, False)	# starts the clock low
		gpio.output(cspin, False)		# bring CS low
		commandout = adcnum
		commandout |= 0x18			# start bit and single-ended bit
		commandout <<= 3			# we only need to send 5 bits
		for i in range(5):
			if (commandout & 0x80):
				gpio.output(mosipin, True)
			else:
				gpio.output(mosipin, False)
			commandout <<= 1
			gpio.output(clockpin, True)
			gpio.output(clockpin, False)
		adcout = 0
		# read one empty bit, one null bit, and 10 adc bits
		for i in range(12):
			gpio.output(clockpin, True)
			gpio.output(clockpin, False)
			adcout <<= 1
			if (gpio.input(misopin)):
				adcout |= 0x1
		gpio.output(cspin, True)
		adcout >>= 1
		return adcout
