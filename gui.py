#!/usr/bin/env python3
# A gui app for a medical dispensation unit
# By: Us

# to run RFID scanner, you must stop the system from using the port
# run 'sudo raspi-config', go under advanced options
# select the serial options and disable it.

"""each screen is it's own class.
initialize() definition in each class sets up the tkinter widgets.
specific details are commented in each class.
"""

from tkinter import *
import tkinter as tk
import RPi.GPIO as gpio
import multiprocessing as mp
import threading as thread
import logging
import serial
import time

# imports of our files and classes
import pots
import dbReset
import db_connect

# logging config
logging.basicConfig(level=logging.DEBUG, format='(%(threadName)s) %(message)s',)

# the wait screen for a RFID scan
# calls appropriate screen based on rfid scan
# and corresponding auth level
class WaitForScan:
	def __init__(self, master):
		self.master = master
		self.initialize()
	def initialize(self):
		# puts the parent widget on a grid. basically centers all of the widgets
		self.master.grid()
		self.master.rowconfigure(0, weight=1)
		self.master.columnconfigure(0, weight=1)
		# label for telling people to scan
		lbl = tk.Label(self.master, text='Please Scan ID Bracelet')
		lbl.configure(font='Arial 20 bold')
		btnQuit = tk.Button(self.master, text='Quit', command=self.logout)
		btnQuit.configure(font='Arial 20 bold')
		#btnStocker = tk.Button(self.master, text='Stocker Scan', command=self.stocker)
		#btnStocker.configure(font='Arial 20 bold')
		#btnDoctor = tk.Button(self.master, text='Doctor Scan', command=self.doctor)
		#btnDoctor.configure(font='Arial 20 bold')
		btnMode1 = tk.Button(self.master, text='Mode 0', command=lambda: self.changeMode(0))
		btnMode1.configure(font='Arial 20 bold')
		btnMode2 = tk.Button(self.master, text='Mode 1', command=lambda: self.changeMode(1))
		btnMode2.configure(font='Arial 20 bold')
		btnMode1.grid(row=1, column=0)
		btnMode2.grid(row=1, column=1)
		lbl.grid(row=0, column=0)
		btnQuit.grid(row=3, column=0)
		#btnStocker.grid(row=2, column=0)
		#btnDoctor.grid(row=1, column=1)
		self.waitForScan()
	# a function to change the mode of the database.
	def changeMode(self, mode):
		db.mode = mode
	# called when the auth level of the rfid scan is for a stocker
	# opens the screen for waiting for the box open
	def stocker(self):
		# opens the window for stocking drugs
		self.newWindow = tk.Toplevel(self.master)
		self.newWindow.overrideredirect(True)
		self.newWindow.geometry('%dx%d+0+0' % (self.master.winfo_screenwidth(), self.master.winfo_screenheight()))
		self.app = ValidRFIDScanStock(self.newWindow, self.master)
	# called when the auth level of the rfid scan is for a medical professional
	# opens the screen for waiting for the box open
	def doctor(self):
		# opens the window for taking drugs
		self.newWindow = tk.Toplevel(self.master)
		self.newWindow.overrideredirect(True)
		self.newWindow.geometry('%dx%d+0+0' % (self.master.winfo_screenwidth(), self.master.winfo_screenheight()))
		self.app = ValidRFIDScanMed(self.newWindow, self.master)
	# called when the auth level of the rfid scan is for a admin
	# goes straight to the admin screen
	def admin(self):
		# opens the window for failed RFID scans
		self.newWindow = tk.Toplevel(self.master)
		self.newWindow.overrideredirect(True)
		self.newWindow.geometry('%dx%d+0+0' % (self.master.winfo_screenwidth(), self.master.winfo_screenheight()))
		self.app = Admin(self.newWindow)
	# a function to wait for the rfid scan
	# checks the auth level and then calls the appropriate screen
	def waitForScan(self):
		# checks to make sure there is something to read for the RFID scanner
		if (ser.inWaiting() > 0):
			try:
				# reads input from the scanner
				string = ser.read(12)
				if len(string) == 0:
					pass
				else:
					# if it did read something, it chops off the beginning to get the id string
					string = string[1:11]
					string = string.decode("utf-8")
					print (string)
					# checks to see if the rfid tag is in the database
					if db.checkInRFID(string):
						# sets user equal to the important values
						user.rfid = string
						user.name = db.getNameByRfid(user.rfid)
						user.auth = db.getAuthByRFID(user.rfid)
						# uses the auth level to send the user to the right page
						if user.auth == 0:
							self.stocker()
						if user.auth == 1:
							self.doctor()
						if user.auth == 2:
							self.admin()
					else:
						# opens new window to say the rfid scan failed
						# cannot be in new definition in order to continue running
						# RFID scanner in this loop
						self.newWindow = tk.Toplevel(self.master)
						self.newWindow.overrideredirect(True)
						self.newWindow.geometry('%dx%d+0+0' % (self.master.winfo_screenwidth(), self.master.winfo_screenheight()))
						self.app = FailedScan(self.newWindow)
						ser.flushInput()
					ser.flushInput()
			except KeyboardInterrupt:
				self.master.after(1, self.waitForScan)
			except serial.SerialException:
				self.master.after(1, self.waitForScan)
			ser.flushInput()
		# after method calls this definition to create RFID loop
		self.master.after(1, self.waitForScan)
	# function to called to log out of program
	def logout(self):
		db.endGPS()
		self.master.destroy()

# a screen to tell the user to open the box 
# medical personel oriented
class ValidRFIDScanMed:
	def __init__(self, master, parent):
		self.master = master
		self.parent = parent
		self.initialize()
	def initialize(self):
		# centers the widgets
		self.master.grid()
		self.master.columnconfigure(0, weight=1)
		self.master.rowconfigure(0, weight=1)
		# puts the various widgets on the grid
		lblFail = tk.Label(self.master, text='RFID Scan Valid. \nPlease Open Box')
		lblFail.configure(font='Arial 20 bold')
		lblFail.grid()
		# call to wait for the box to be opened
		self.waitForBoxOpen()
	# function to wait for the tilt sensor to register as opened
	def waitForBoxOpen(self):
		# checks the box status set by BoxStatus class
		if boxStatus.boxOpened == True:
			self.quit()
		# calls the looping function
		self.master.after(200, self.waitForBoxOpen)
	# closes the window and opens the screen for medical professionals
	def quit(self):
		self.master.destroy()
		self.newWindow = tk.Toplevel(self.parent)
		self.newWindow.overrideredirect(True)
		self.newWindow.geometry('%dx%d+0+0' % (self.parent.winfo_screenwidth(), self.parent.winfo_screenheight()))
		self.app = Medical(self.newWindow, self.parent)

# a screen to tell user to open box
# stocking personnel oriented
class ValidRFIDScanStock:
	def __init__(self, master, parent):
		self.master = master
		self.parent = parent
		self.initialize()
	def initialize(self):
		# centers the widget
		self.master.grid()
		self.master.columnconfigure(0, weight=1)
		self.master.rowconfigure(0, weight=1)
		# places the widgets on the grid
		lblFail = tk.Label(self.master, text='RFID Scan Valid. \nPlease Open Box')
		lblFail.configure(font='Arial 20 bold')
		lblFail.grid()
		# call to wait for box open
		self.waitForBoxOpen()
	# function to wait for the tilt sensor to register the box as open
	def waitForBoxOpen(self):
		# checks the box status set by BoxStatus
		if boxStatus.boxOpened == True:
			self.quit()
		# calls the looping function
		self.master.after(200, self.waitForBoxOpen)
	# closes the window and opens the window for stocking
	def quit(self):
		self.master.destroy()
		self.newWindow = tk.Toplevel(self.parent)
		self.newWindow.overrideredirect(True)
		self.newWindow.geometry('%dx%d+0+0' % (self.parent.winfo_screenwidth(), self.parent.winfo_screenheight()))
		self.app = Stocking(self.newWindow, self.parent)

# a quick window for a failed scan
class FailedScan:
	def __init__(self, master):
		self.master = master
		self.initialize()
	def initialize(self):
		# centers the widgets
		self.master.grid()
		self.master.columnconfigure(0, weight=1)
		self.master.rowconfigure(0, weight=1)
		# places the widgets on the grid
		lblFail = tk.Label(self.master, text='RFID not recognized')
		lblFail.configure(font='Arial 20 bold')
		lblFail.grid()
		# waits 3 seconds then calls the quit function
		self.master.after(3000, self.quit)
	# destroys the window
	def quit(self):
		self.master.destroy()

# a screen for a medical person opening a bin twice
# might add a button with a question on it
class InvalidBinOpenMed:
	def __init__(self, master):
		self.master = master
		self.initialize()
	def initialize(self):
		self.master.grid()
		self.master.columnconfigure(0, weight=1)
		self.master.rowconfigure(0, weight=1)
		lblFail = tk.Label(self.master, text='Bin is Empty')
		lblFail.configure(font='Arial 20 bold')
		lblFail.grid()
		self.master.after(3000, self.quit)
	def quit(self):
		self.master.destroy()

# a screen for a stocker opening a bin multiple times
# might add a button with a question on it
class InvalidBinOpenStock:
	def __init__(self, master):
		self.master = master
		self.initialize()
	def initialize(self):
		self.master.grid()
		self.master.columnconfigure(0, weight=1)
		self.master.rowconfigure(0, weight=1)
		lblFail = tk.Label(self.master, text='Bin is Full')
		lblFail.configure(font='Arial 20 bold')
		lblFail.grid()
		self.master.after(3000, self.quit)
	def quit(self):
		self.master.destroy()

# the class for the admin screen
class Admin:
	def __init__(self, master):
		self.master = master
		self.initialize()
	def initialize(self):
		# sets the master grid
		self.master.grid()
		self.master.rowconfigure(0, weight=1)
		self.master.columnconfigure(0, weight=1)
		# the only button
		btnReset = tk.Button(self.master, text='RESET', command=self.resetDB)
		btnReset.configure(font='Arial 20 bold', height=3, width=30)
		# putting the button on page
		btnReset.grid()
	def resetDB(self):
		# calls the python reset script to reset the database
		db.resetBox()
		user.logout()
		newWindow = tk.Toplevel(self.master)
		newWindow.overrideredirect(True)
		newWindow.geometry('%dx%d+0+0' % (winfo_screenwidth(), winfo_screenheight()))
		self.app = WaitForScan(self.newWindow)

# the class for the screen for medical workers
class Medical:
	def __init__(self, master, topWindow):
		self.master = master
		self.topWindow = topWindow
		self.initialize()
	def initialize(self):
		# setting up the grid
		self.master.grid()
		self.master.columnconfigure(0, weight=1)
		self.master.columnconfigure(1, weight=3)
		self.master.rowconfigure(0, weight=1)
		self.master.rowconfigure(1, weight=1)
		self.master.rowconfigure(2, weight=1)
		self.master.rowconfigure(3, weight=1)
		self.master.rowconfigure(4, weight=1)
		self.master.rowconfigure(5, weight=1)
		self.master.rowconfigure(6, weight=1)
		self.master.rowconfigure(7, weight=1)
		# pulling dictionaries of names and amounts
		self.names = db.getNames()
		self.amts = db.getCounts()
		self.lblText = []
		# adding the labels based on contents of names and amts
		for i in range(1, len(self.names)):
			self.lblText.append(StringVar())
			self.lblText[i-1].set("Bin " + str(i) + " - " + str(self.names[int(i)]) + ": " + str(self.amts[int(i)]))
			lbl = tk.Label(self.master, textvariable=self.lblText[i-1])
			lbl.configure(font='Arial 14 bold', anchor='w', justify=LEFT)
			lbl.grid(row=(i-1), column=0, sticky='NSEW')
		# other labels and buttons
		lblMain = tk.Label(self.master, text='Please Take Drugs as Needed')
		lblMain.configure(font='Arial 14 bold')
		btnLogout = tk.Button(self.master, text='Logout', command=self.logout)
		btnLogout.configure(font='Arial 14 bold')
		lblMain.grid(row=2, column=2)
		btnLogout.grid(row=5, column=2, sticky='NSEW')
		self.waitForBin()
	# function to tell database a bin has been taken from
	def takeFromBin(self, pos):
		# take from a single bin defined by 'pos'
		if self.amts[int(pos)] >= 1:
			update = db.updateQty(int(pos), 0)
			if update:
				self.amts = db.getCounts()
				self.lblText[pos-1].set("Bin " + str(pos) + " - " + str(self.names[int(pos)]) + ": " + str(self.amts[int(pos)]))
				print ("Took from bin " + str(pos))
			else:
				print ("Taking failed: " + str(update))
	# a function to call the screen to wait for box close
	# performed as a logout
	def logout(self):
		self.newWindow = tk.Toplevel(self.master)
		self.newWindow.overrideredirect(True)
		self.newWindow.geometry('%dx%d+0+0' % (self.master.winfo_screenwidth(), self.master.winfo_screenheight()))
		self.app = LogoutBoxClose(self.newWindow, self.topWindow)
	# a function to wait for a bin to be opened
	# pulls list from the queue
	# queue filled by pots.py
	def waitForBin(self):
		if not queue.empty():
			bins = queue.get()
			for i in range(0,len(bins)):
				if bins[i]:
					self.takeFromBin(i+1)
		# looping function
		self.master.after(200, self.waitForBin)

# the class for the screen for stocking the box
class Stocking:
	def __init__(self, master, topWindow):
		self.master = master
		self.topWindow = topWindow
		self.initialize()
	def initialize(self):
		# setting up the grid
		self.master.grid()
		self.master.columnconfigure(0, weight=1)
		self.master.columnconfigure(1, weight=1)
		self.master.columnconfigure(2, weight=2)
		self.master.rowconfigure(0, weight=1)
		self.master.rowconfigure(1, weight=1)
		self.master.rowconfigure(2, weight=1)
		self.master.rowconfigure(3, weight=1)
		self.master.rowconfigure(4, weight=1)
		self.master.rowconfigure(5, weight=1)
		self.master.rowconfigure(6, weight=1)
		self.master.rowconfigure(7, weight=1)
		# pulling dictionaries of names and amounts
		self.names = db.getNames()
		self.amts = db.getCounts()
		self.btn = []
		self.lblText = []
		# adding the labels based on contents of names and amts
		for i in range(1, len(self.names)):
			self.lblText.append(StringVar())
			self.lblText[i-1].set("Bin " + str(i) + " - " + str(self.names[int(i)]) + ": " + str(self.amts[int(i)]))
			lbl = tk.Label(self.master, textvariable=self.lblText[i-1])
			lbl.configure(font='Arial 14 bold', anchor='w', justify=LEFT)
			lbl.grid(row=(i-1), column=0, sticky='NSEW')
			if self.amts[i] == 0:
				self.btn.append(tk.Button(self.master, text='Add One'))
				self.btn[len(self.btn)-1].configure(font='Arial 14 bold', command=lambda btn=self.btn[len(self.btn)-1]: self.addToBin(btn))
				self.btn[len(self.btn)-1].grid(row=(i-1), column=1, sticky='NSEW')
		# other labels and buttons
		lblMain = tk.Label(self.master, text='Please Add Drugs as Needed')
		lblMain.configure(font='Arial 14 bold', wraplength=100, justify=CENTER)
		btnLogout = tk.Button(self.master, text='Logout', command=self.logout)
		btnLogout.configure(font='Arial 14 bold')
		lblMain.grid(row=2, column=2)
		btnLogout.grid(row=5, column=2, sticky='NSEW')
		self.waitForBin()
	# function to tell database a bin has been added to
	# called by button
	def addToBin(self, obj):
		loc = obj.grid_info()
		if self.amts[int(loc['row'])+1] == 0:
			db.updateQty(int(loc['row'])+1, 1)
			obj.grid_remove()
	# function to tell database a bin has been added to
	# called by waitForBin based on bin opened
	def addToBinNum(self, pos):
		if self.amts[int(pos)] == 0:
			update = db.updateQty(int(pos), 1)
			if update:
				self.amts = db.getCounts()
				self.lblText[pos-1].set("Bin " + str(pos) + " - " + str(self.names[int(pos)]) + ": " + str(self.amts[int(pos)]))
				print ("Added to bin " + str(pos))
			else:
				print ("Adding failed: " + str(update))
	# function to call the screen to wait for box close
	def logout(self):
		self.newWindow = tk.Toplevel(self.master)
		self.newWindow.overrideredirect(True)
		self.newWindow.geometry('%dx%d+0+0' % (self.master.winfo_screenwidth(), self.master.winfo_screenheight()))
		self.app = LogoutBoxClose(self.newWindow, self.topWindow)
	# a function to wait for a bin to be opened
	# pulls list from the queue
	# queue filled by pots.py
	def waitForBin(self):
		if not queue.empty():
			bins = queue.get()
			for i in range(0, len(bins)):
				if bins[i]:
					self.addToBinNum(i+1)
		self.master.after(200, self.waitForBin)

# a screen to wait for box to be closed after logout
class LogoutBoxClose:
	"""a screen that waits for the box to be closed after the user logs out
	once box closes, clears user data, destroys app, and restarts app at WaitForRFID screen
	recalls main() to restart app.  all threads continue running
	"""
	def __init__(self, master, topWindow):
		self.master = master
		self.topWindow = topWindow
		self.initialize()
	def initialize(self):
		# sets up the grid
		self.master.grid()
		self.master.columnconfigure(0, weight=1)
		self.master.rowconfigure(0, weight=1)
		# places widgets on grid
		lblFail = tk.Label(self.master, text='Please Close Box \nTo Complete Logout')
		lblFail.configure(font='Arial 20 bold')
		lblFail.grid()
		# calls function to wait for box close
		self.waitForBoxClose()
	def waitForBoxClose(self):
		"""Waits for the tilt sensor to register the box as closed
		Checks if bins have been opened in the mean time
		"""
		# checks boxStatus based on BoxStatus
		if boxStatus.boxOpened == False:
			self.quit()
		if not queue.empty():
			bins = queue.get()
			for i in range(0, len(bins)-1):
				if bins[i]:
					self.takeFromBin(i+1)
		# looping function
		self.master.after(200, self.waitForBoxClose)
	def takeFromBin(self, pos):
		"""function to remove drugs from a bin, in case user opens a bin.
		arg:	pos = position of the bin opened
		"""
		if getCountInBin(pos):
			db.insertItems(pos, (0-db.getCountInBin()))
	# logs out the user
	# closes app and then recalls it through main()
	def quit(self):
		user.logout()
		self.topWindow.destroy()
		main()

# class to wait for a box to be opened
# unused and to be deleted
class WaitForBoxOpen:
	def __init__(self, master, pos=None):
		self.master = master
		self.pos = pos
		self.initialize()
	def initialize(self):
		# centers the widgets
		self.master.grid()
		self.master.rowconfigure(0, weight=1)
		self.master.columnconfigure(0, weight=1)
		# create label widget for the page
		lblOpen = tk.Label(self.master, text='Please Open a Box')
		lblOpen.configure(font='Arial 20 bold')
		lblOpen.grid()
	def waitForOpen(self):
		# catches whether the program is waiting for any box to be opened
		# or a specific box
		if self.pos is None:
			pass
		else:
			pass
		self.master.after(500, self.waitForOpen)

# these screens will probably go away later
# for now i'm keeping them for testing purposes
# the first screen of the gui
class Gui:
	def __init__(self, master):
		self.master = master
		self.initialize()
	def initialize(self):
		# puts the parent widget on a grid. basically centers all of the buttons
		self.master.grid()
		self.master.rowconfigure(0, weight=1)
		self.master.columnconfigure(0, weight=1)
		# sets the frame on the grid. trying to get bigger buttons...
		self.frame = tk.Frame(self.master)
		self.frame.grid(row=0, column=0)
		self.frame.columnconfigure(0, weight=1)
		self.frame.columnconfigure(1, weight=1)
		self.frame.rowconfigure(0, weight=1)
		self.frame.rowconfigure(1, weight=1)
		# declaration and configuring widgets
		btnAddDrugs = tk.Button(self.frame, text='Add Drugs to Box', command=self.addDrugs)
		btnAddDrugs.configure(height=4, width=25, font='Arial 20 bold')
		btnTakeDrugs = tk.Button(self.frame, text='Take Drugs from Box', command=self.takeDrugs)
		btnTakeDrugs.configure(height=4, width=25, font='Arial 20 bold')
		btnQuit = tk.Button(self.frame, text="Quit", command=self.quit)
		btnQuit.configure(height=2, font='Arial 20 bold')
		# setting the widgets on the grid
		btnAddDrugs.grid(row=0, column=0, sticky='NSEW')
		btnTakeDrugs.grid(row=0, column=1, sticky='NSEW')
		btnQuit.grid(row=1, column=0, columnspan=2, rowspan=2, sticky='NSEW')
	def addDrugs(self):
		# opens a new window for adding drugs
		self.newWindow = tk.Toplevel(self.master)
		self.newWindow.overrideredirect(True)
		self.newWindow.geometry('%dx%d+0+0' % (self.master.winfo_screenwidth(), self.master.winfo_screenheight()))
		self.app = AddDrugs(self.newWindow)
	def takeDrugs(self):
		# opens a new window for withdrawing drugs
		self.newWindow = tk.Toplevel(self.master)
		self.newWindow.overrideredirect(True)
		self.newWindow.geometry('%dx%d+0+0' % (self.master.winfo_screenwidth(), self.master.winfo_screenheight()))
		self.app = TakeDrugs(self.newWindow)
	def quit(self):
		self.master.destroy()

# one potential second screen for adding drugs to the collection
class AddDrugs:
	def __init__(self, master):
		self.master = master
		self.initialize()
	def initialize(self):
		# puts the parent widget on a grid. basically centers all of the buttons
		self.master.grid()
		self.master.rowconfigure(0, weight=1)
		self.master.columnconfigure(0, weight=1)
		self.frame = tk.Frame(self.master)
		self.frame.grid()
		# declaration and configuring widgets
		btnAdd1 = tk.Button(self.frame, text=dc.returnKey(0), command=lambda: self.add(dc.returnKey(0)))
		btnAdd1.configure(height=4, width=25, font='Arial 20 bold')
		btnAdd2 = tk.Button(self.frame, text=dc.returnKey(1), command=lambda: self.add(dc.returnKey(1)))
		btnAdd2.configure(height=4, width=25, font='Arial 20 bold')
		btnAdd3 = tk.Button(self.frame, text=dc.returnKey(2), command=lambda: self.add(dc.returnKey(2)))
		btnAdd3.configure(height=4, width=25, font='Arial 20 bold')
		btnAdd4 = tk.Button(self.frame, text=dc.returnKey(3), command=lambda: self.add(dc.returnKey(3)))
		btnAdd4.configure(height=4, width=25, font='Arial 20 bold')
		btnQuit = tk.Button(self.frame, text="Done", command=self.quit)
		btnQuit.configure(height=2, font='Arial 20 bold')
		# setting the widgets on the grid
		btnAdd1.grid(row=0, column=0)
		btnAdd2.grid(row=0, column=1)
		btnAdd3.grid(row=1, column=0)
		btnAdd4.grid(row=1, column=1)
		btnQuit.grid(row=2, column=0, columnspan=2, sticky='NSEW')
	def add(self, drug):
		# opens new window to choose the amount of drugs to be added
		self.newWindow = tk.Toplevel(self.master)
		self.newWindow.overrideredirect(True)
		self.newWindow.geometry('%dx%d+0+0' % (self.master.winfo_screenwidth(), self.master.winfo_screenheight()))
		self.app = AddDrugsAmount(self.newWindow, drug)
	def quit(self):
		dc.printAmounts()
		self.master.destroy()

# one potential second screen for taking drugs from the collection
class TakeDrugs:
	def __init__(self, master):
		self.master = master
		self.initialize()
	def initialize(self):
		# puts the parent widget on a grid. basically centers all of the buttons
		self.master.grid()
		self.master.rowconfigure(0, weight=1)
		self.master.columnconfigure(0, weight=1)
		self.frame = tk.Frame(self.master)
		self.frame.grid()
		# declaration and configuring widgets
		btnTake1 = tk.Button(self.frame, text=dc.returnKey(0), command=lambda: self.take(dc.returnKey(0)))
		btnTake1.configure(height=4, width=25, font='Arial 20 bold')
		btnTake2 = tk.Button(self.frame, text=dc.returnKey(1), command=lambda: self.take(dc.returnKey(1)))
		btnTake2.configure(height=4, width=25, font='Arial 20 bold')
		btnTake3 = tk.Button(self.frame, text=dc.returnKey(2), command=lambda: self.take(dc.returnKey(2)))
		btnTake3.configure(height=4, width=25, font='Arial 20 bold')
		btnTake4 = tk.Button(self.frame, text=dc.returnKey(3), command=lambda: self.take(dc.returnKey(3)))
		btnTake4.configure(height=4, width=25, font='Arial 20 bold')
		btnQuit = tk.Button(self.frame, text="Done", command=self.quit)
		btnQuit.configure(height=2, font='Arial 20 bold')
		# setting the widgets on the grid
		btnTake1.grid(row=0, column=0)
		btnTake2.grid(row=0, column=1)
		btnTake3.grid(row=1, column=0)
		btnTake4.grid(row=1, column=1)
		btnQuit.grid(row=2, column=0, columnspan=2, sticky='NSEW')
	def take(self, drug):
		# opens new window to choose amount to drugs to be taken
		self.newWindow = tk.Toplevel(self.master)
		self.newWindow.overrideredirect(True)
		self.newWindow.geometry('%dx%d+0+0' % (self.master.winfo_screenwidth(), self.master.winfo_screenheight()))
		self.app = TakeDrugsAmount(self.newWindow, drug)
	def quit(self):
		dc.printAmounts()
		self.master.destroy()

# a class for choosing the amount of drugs to be taken
class TakeDrugsAmount:
	def __init__(self, master, drug):
		self.master = master
		# declares amount var and the StringVar that will fill label
		self.amount = 1
		self.var = StringVar()
		self.var.set(str(self.amount))
		self.initialize(drug)
	def initialize(self, drug):
		# puts the parent widget on a grid. basically centers all of the buttons
		self.master.grid()
		self.master.rowconfigure(0, weight=1)
		self.master.columnconfigure(0, weight=1)
		self.frame = tk.Frame(self.master)
		self.frame.grid()
		# declaration and configuring widgets
		# quick choices
		btnQuick1 = tk.Button(self.frame, text='Take 1', command=lambda: self.take(drug, self.amount))
		btnQuick1.configure(height=3, width=20, font='Arial 20 bold')
		btnQuick2 = tk.Button(self.frame, text='Take 2', command=lambda: self.take(drug, self.amount))
		btnQuick2.configure(height=3, width=20, font='Arial 20 bold')
		btnQuick3 = tk.Button(self.frame, text='Take 3', command=lambda: self.take(drug, self.amount))
		btnQuick3.configure(height=3, width=20, font='Arial 20 bold')
		btnQuick4 = tk.Button(self.frame, text='Take 4', command=lambda: self.take(drug, self.amount))
		btnQuick4.configure(height=3, width=20, font='Arial 20 bold')
		# spinner for different amounts
		lblAmt = tk.Label(self.frame, textvariable=self.var)
		lblAmt.configure(height=2, width=10, font='Arial 20 bold')
		btnIncrease = tk.Button(self.frame, text='Up', command=self.increase)
		btnIncrease.configure(height=2, width=10, font='Arial 20 bold')
		btnDecrease = tk.Button(self.frame, text='Down', command=self.decrease)
		btnDecrease.configure(height=2, width=10, font='Arial 20 bold')
		btnAccept = tk.Button(self.frame, text='Submit', command=lambda: self.take(drug, self.amount))
		btnAccept.configure(height=3, width=25, font='Arial 20 bold')
		btnCancel = tk.Button(self.frame, text='Cancel', command=self.cancel)
		btnCancel.configure(height=3, width=20, font='Arial 20 bold')
		# setting the widgets on the grid
		btnQuick1.grid(row=0, column=0, rowspan=3, columnspan=2)
		btnQuick2.grid(row=0, column=2, rowspan=3, columnspan=2)
		btnQuick3.grid(row=3, column=0, rowspan=3, columnspan=2)
		btnQuick4.grid(row=3, column=2, rowspan=3, columnspan=2)
		lblAmt.grid(row=0, column=4, rowspan=2)
		btnIncrease.grid(row=2, column=4, rowspan=2)
		btnDecrease.grid(row=4, column=4, rowspan=2)
		btnAccept.grid(row=6, column=0, rowspan=3, columnspan=3)
		btnCancel.grid(row=6, column=3, rowspan=3, columnspan=2)
	def take(self, drug, amount):
		dc.take(drug, amount)
		self.master.destroy()
	def cancel(self):
		self.master.destroy()
	# definitions to operate spinner
	def increase(self):
		if self.amount == 10:
			self.amount = 0
		else:
			self.amount += 1
		self.var.set(str(self.amount))
	def decrease(self):
		if self.amount == 1:
			self.amount = 10
		else:
			self.amount -= 1
		self.var.set(str(self.amount))

# a class for choosing the amount of drugs to add.
class AddDrugsAmount:
	def __init__(self, master, drug):
		self.master = master
		# declares amount var and StringVar that will fill label
		self.amount = 1
		self.var = StringVar()
		self.var.set(str(self.amount))
		self.initialize(drug)
	def initialize(self, drug):
		# puts the parent widget on a grid. basically centers all of the buttons
		self.master.grid()
		self.master.rowconfigure(0, weight=1)
		self.master.columnconfigure(0, weight=1)
		self.frame = tk.Frame(self.master)
		self.frame.grid()
		# declaration and configuring widgets
		# quick choices
		btnQuick1 = tk.Button(self.frame, text='Add 1', command=lambda: self.add(drug, self.amount))
		btnQuick1.configure(height=3, width=20, font='Arial 20 bold')
		btnQuick2 = tk.Button(self.frame, text='Add 2', command=lambda: self.add(drug, self.amount))
		btnQuick2.configure(height=3, width=20, font='Arial 20 bold')
		btnQuick3 = tk.Button(self.frame, text='Add 3', command=lambda: self.add(drug, self.amount))
		btnQuick3.configure(height=3, width=20, font='Arial 20 bold')
		btnQuick4 = tk.Button(self.frame, text='Add 4', command=lambda: self.add(drug, self.amount))
		btnQuick4.configure(height=3, width=20, font='Arial 20 bold')
		# spinner for different amounts
		lblAmt = tk.Label(self.frame, textvariable=self.var)
		lblAmt.configure(height=2, width=10, font='Arial 20 bold')
		btnIncrease = tk.Button(self.frame, text='Up', command=self.increase)
		btnIncrease.configure(height=2, width=10, font='Arial 20 bold')
		btnDecrease = tk.Button(self.frame, text='Down', command=self.decrease)
		btnDecrease.configure(height=2, width=10, font='Arial 20 bold')
		btnAccept = tk.Button(self.frame, text='Submit', command=lambda: self.add(drug, self.amount))
		btnAccept.configure(height=3, width=30, font='Arial 20 bold')
		btnCancel = tk.Button(self.frame, text='Cancel', command=self.cancel)
		btnCancel.configure(height=3, width=20, font='Arial 20 bold')
		# setting the widgets on the grid
		btnQuick1.grid(row=0, column=0, rowspan=3, columnspan=2)
		btnQuick2.grid(row=0, column=2, rowspan=3, columnspan=2)
		btnQuick3.grid(row=3, column=0, rowspan=3, columnspan=2)
		btnQuick4.grid(row=3, column=2, rowspan=3, columnspan=2)
		lblAmt.grid(row=0, column=4, rowspan=2)
		btnIncrease.grid(row=2, column=4, rowspan=2)
		btnDecrease.grid(row=4, column=4, rowspan=2)
		btnAccept.grid(row=6, column=0, rowspan=3, columnspan=3)
		btnCancel.grid(row=6, column=3, rowspan=3, columnspan=2)
	def add(self, drug, amount):
		dc.add(drug, amount)
		self.master.destroy()
	def cancel(self):
		self.master.destroy()
	# definitions to operate spinner
	def increase(self):
		if self.amount == 10:
			self.amount = 1
		else:
			self.amount += 1
		self.var.set(str(self.amount))
	def decrease(self):
		if self.amount == 1:
			self.amount = 10
		else:
			self.amount -= 1
		self.var.set(str(self.amount))

# a collection to stored and manipulate the amount of drugs
# most likely will be changed to operate on sql queries
# to be fully removed
class DrugsCollection:
	def __init__(self):
		#self.box = db.getStock()
		self.drugs = {'vicodin': 0, 'epenephrin': 0, 'robetussin': 0, 'lithium': 0}
	def add(self, drug, amt):
		self.drugs[drug] = self.drugs[drug] + int(amt)
	def take(self, drug, amt):
		self.drugs[drug] = self.drugs[drug] - int(amt)
	def printAmounts(self):
		for keys, values in self.drugs.items():
			print(keys)
			print(values)
	def returnKey(self, pos):
		keys = list(self.drugs.keys())
		return keys[int(pos)]

# a class that contains the info of the user
# based on rfid scan
class User:
	def __init__(self):
		self.rfid = False
		self.auth = False
		self.name = False
	# sets user's rfid scan, auth level, and name
	def setUser(self, ident, auth, name):
		self.rfid = ident
		self.auth = auth
		self.name = name
	# might be able to delete this
	# checks the rfid scan in the database
	def checkRFID(self, rfid):
		user = db.checkInRFID(rfid)
		if user == False:
			return False
		else:
			return True
	# this to be added later if needed
	def checkAuth(self):
		pass
	# returns rfid id
	def getRFID(self):
		return self.rfid
	# returns auth level
	def getAuth(self):
		return self.auth
	# returns name of user
	def getName(self):
		return self.name
	# logs out user and sets variables to False
	def logout(self):
		self.rfid = False
		self.auth = False
		self.name= False

# still hoping to get this going
# supposed to close current window when esc is pressed
def close(event):
	event.widget.logout()

# the class to be called when the tilt sensor event is detected
class BoxStatus:
	def __init__(self):
		self.boxOpened = False
		self.lastValue = False
		self.numberChecks = 20
		self.delay = .01
		self.valueChanged = False
	# called by event detect on gpio channel
	# checks gpio.input self.numberChecks of times
	# if all checks came back the same, it calls boxOpen or boxClose
	def boxOpenClose(self, channel):
		self.valueChanged = False
		self.channel = channel
		for i in range(self.numberChecks):
			if not self.valueChanged:
				self.lastValue = gpio.input(channel)
				self.t = thread.Timer(self.delay, self.debounce)
				self.t.start()
				time.sleep(self.delay)
			else:
				break
		if not self.valueChanged:
			if self.lastValue and not self.boxOpened:
				self.boxOpen()
			elif not self.lastValue and self.boxOpened:
				self.boxClose()
			else:
				logging.debug("?- lastValue=" + str(self.lastValue) + ", boxOpened = " + str(self.boxOpened))
	# called by boxOpenClose
	# checks to see if lastValue is the same as
	# the current value of gpio.input
	def debounce(self):
		if gpio.input(self.channel) != self.lastValue:
			self.valueChanged = True
		try:
			self.t.cancel()
			#logging.debug("t cancelled now ? " + str(thread.enumerate()))
		except:
			pass
	# sets status of box to 'open'
	def boxOpen(self):
		logging.debug("Box Open")
		self.boxOpened = True
	# sets status of box to 'close'
	def boxClose(self):
		logging.debug("Box Close")
		self.boxOpened = False


# the main loop for the gui
def main():
	#resetdb = dbReset.CreateDB()
	#resetdb.reset()
	root = tk.Tk()
	root.overrideredirect(True)
	root.geometry('%dx%d+0+0' % (root.winfo_screenwidth(), root.winfo_screenheight()))
	root.title('Medical Dispensation')
	root.bind_all("<Escape>", close)
	app = WaitForScan(root)
	root.mainloop()

# sets mode of gpio board
gpio.setmode(gpio.BCM)

# creates some objects to be used in program
db = db_connect.DbMedbox(False, False)
user = User()
dc = DrugsCollection()
boxStatus = BoxStatus()
pots = pots.potentiometers()

# opens the serial port for the RFID reader
ser = serial.Serial('/dev/ttyAMA0', 9600, timeout=0.5)

# sets up channel 7 for tilt sensor
# adds event detect for registering a change in tilt sensor
gpio.setup(7, gpio.IN, pull_up_down=gpio.PUD_UP)
gpio.add_event_detect(7, gpio.BOTH, callback=boxStatus.boxOpenClose, bouncetime=200)

# checks to make sure another program isn't calling it
if __name__ == '__main__':
	try:
		# opens queue and starts pots process
		queue = mp.Queue()
		p = mp.Process(name='pots', target=pots.checkPotentiometers, args=(queue,))
		p.daemon = True
		p.start()
		main()
	except KeyboardInterrupt:
		print ("Keyboard Interrupt!")
	finally:
		gpio.cleanup()
