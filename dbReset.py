# Run file to install/override db `medbox` if your version older then:
# LAST DB CHANGES: 2015-06-04 21:33:42

import pymysql

class CreateDB():
    def reset(self):
        try:
            self.cnx=pymysql.connect(user="root", passwd="RRCCpi2DC",
                host="127.0.0.1", db="medbox", port=3306)
        except:
            try:
                self.cnx=pymysql.connect(user="root", passwd="RRCCpi2DC",
                    host="127.0.0.1", port=3306)
            except:
                self.cnx=pymysql.connect(user="root",host="127.0.0.1", port=3306)                
        self.cursor=self.cnx.cursor()
        self.runSql()
        
    def runSql(self):
        #-- MySQL dump 10.13  Distrib 5.6.21, for Win32 (x86)
        print('Processing File From: MySQL dump 10.13  Distrib 5.6.21, for Win32 (x86)')
        #-- Host: localhost    Database: medbox
        print('Processing File From: Host: localhost    Database: medbox')        #-- ------------------------------------------------------
        print('Processing File From: ------------------------------------------------------')        #-- Server version	5.6.21
        print('Processing File From: Server version	5.6.21')

        self.cursor.execute("/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;")

        self.cursor.execute("/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;")

        self.cursor.execute("/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;")

        self.cursor.execute("/*!40101 SET NAMES utf8 */;")

        self.cursor.execute("/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;")

        self.cursor.execute("/*!40103 SET TIME_ZONE='+00:00' */;")

        self.cursor.execute("/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;")

        self.cursor.execute("/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;")

        self.cursor.execute("/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;")

        self.cursor.execute("/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;")


        #-- Current Database: `medbox`
        print('Processing File From: Current Database: `medbox`')


        self.cursor.execute("CREATE DATABASE /*!32312 IF NOT EXISTS*/ `medbox` /*!40100 DEFAULT CHARACTER SET latin1 */;")


        self.cursor.execute("USE `medbox`;")


        #-- Table structure for table `bin`
        print('Processing File From: Table structure for table `bin`')


        self.cursor.execute("DROP TABLE IF EXISTS `bin`;")

        self.cursor.execute("/*!40101 SET @saved_cs_client     = @@character_set_client */;")

        self.cursor.execute("/*!40101 SET character_set_client = utf8 */;")

        self.cursor.execute("""CREATE TABLE `bin` (
          `count` tinyint(3) unsigned NOT NULL DEFAULT '0',
          `angle` float(6,3) unsigned NOT NULL DEFAULT '0.000',
          `drug_ndc` bigint(10) unsigned DEFAULT NULL,
          `equipt_upn` varchar(20) DEFAULT NULL,
          `box_id` bigint(20) unsigned NOT NULL,
          `pos` tinyint(1) unsigned NOT NULL DEFAULT '0',
          `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
          PRIMARY KEY (`id`),
          KEY `fk_drug_ndc` (`drug_ndc`),
          KEY `fk_equipt_upn` (`equipt_upn`),
          KEY `fk_box_id` (`box_id`),
          CONSTRAINT `fk_box_id` FOREIGN KEY (`box_id`) REFERENCES `box` (`id`),
          CONSTRAINT `fk_drug_ndc` FOREIGN KEY (`drug_ndc`) REFERENCES `drug` (`ndc`),
          CONSTRAINT `fk_equipt_upn` FOREIGN KEY (`equipt_upn`) REFERENCES `equipt` (`upn`)
        ) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
        """)

        self.cursor.execute("/*!40101 SET character_set_client = @saved_cs_client */;")


        #-- Dumping data for table `bin`
        print('Processing File From: Dumping data for table `bin`')


        self.cursor.execute("LOCK TABLES `bin` WRITE;")

        self.cursor.execute("/*!40000 ALTER TABLE `bin` DISABLE KEYS */;")

        self.cursor.execute("INSERT INTO `bin` VALUES (1,0.000,NULL,'testupn',1,1,2),(1,0.000,2169519801,NULL,1,2,3),(1,0.000,5258496604,NULL,1,3,4),(1,0.000,5212524641,NULL,1,4,5),(1,0.000,5515495575,NULL,1,5,6),(1,0.000,6332318500,NULL,1,6,7),(1,0.000,6332330201,NULL,1,7,8),(1,0.000,6584176302,NULL,1,8,9),(1,0.000,5428810310,NULL,1,9,10);")

        self.cursor.execute("/*!40000 ALTER TABLE `bin` ENABLE KEYS */;")

        self.cursor.execute("UNLOCK TABLES;")

        self.cursor.execute("/*!50003 SET @saved_cs_client      = @@character_set_client */ ;")

        self.cursor.execute("/*!50003 SET @saved_cs_results     = @@character_set_results */ ;")

        self.cursor.execute("/*!50003 SET @saved_col_connection = @@collation_connection */ ;")

        self.cursor.execute("/*!50003 SET character_set_client  = cp850 */ ;")

        self.cursor.execute("/*!50003 SET character_set_results = cp850 */ ;")

        self.cursor.execute("/*!50003 SET collation_connection  = cp850_general_ci */ ;")

        self.cursor.execute("/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;")

        self.cursor.execute("/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;")

        #note on pywrapsql.py - I need to strip delimiter lines and parse
        #create triggers to remove /*! sql system instructs from CREATE TRIGGER line and end of last END 
        self.cursor.execute("""CREATE TRIGGER InsertBinEitherDrugEquipt BEFORE INSERT ON bin
        FOR EACH ROW
        BEGIN
        IF NEW.drug_ndc IS NOT NULL AND NEW.equipt_upn IS NOT NULL THEN
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT="CANNOT INSERT BOTH DRUG AND EQUIPTMENT IN SAME BIN";
        ELSEIF NEW.count > 0  AND NEW.drug_ndc IS NULL AND NEW.equipt_upn IS NULL THEN 
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'INSERT bin WITH ITEM(S) REQUIRES EITHER A drug_ndc OR A equipt_upn'; 
        END IF; END""")

        self.cursor.execute("/*!50003 SET sql_mode              = @saved_sql_mode */ ;")

        self.cursor.execute("/*!50003 SET character_set_client  = @saved_cs_client */ ;")

        self.cursor.execute("/*!50003 SET character_set_results = @saved_cs_results */ ;")

        self.cursor.execute("/*!50003 SET collation_connection  = @saved_col_connection */ ;")

        self.cursor.execute("/*!50003 SET @saved_cs_client      = @@character_set_client */ ;")

        self.cursor.execute("/*!50003 SET @saved_cs_results     = @@character_set_results */ ;")

        self.cursor.execute("/*!50003 SET @saved_col_connection = @@collation_connection */ ;")

        self.cursor.execute("/*!50003 SET character_set_client  = cp850 */ ;")

        self.cursor.execute("/*!50003 SET character_set_results = cp850 */ ;")

        self.cursor.execute("/*!50003 SET collation_connection  = cp850_general_ci */ ;")

        self.cursor.execute("/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;")

        self.cursor.execute("/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;")

        self.cursor.execute("""
        CREATE TRIGGER UpdateBinEitherDrugEquipt BEFORE UPDATE ON bin
        FOR EACH ROW
        BEGIN
        IF NEW.drug_ndc IS NOT NULL AND NEW.equipt_upn IS NOT NULL THEN
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT="CANNOT UPDATE TO BOTH DRUG AND EQUIPTMENT IN SAME BIN";
        ELSEIF NEW.count > 0  AND NEW.drug_ndc IS NULL AND NEW.equipt_upn IS NULL THEN 
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'UPDATE bin WITH ITEM(S) REQUIRES EITHER A drug_ndc OR A equipt_upn'; 
        END IF; 
        END;
        """)

        
        self.cursor.execute("/*!50003 SET sql_mode              = @saved_sql_mode */ ;")

        self.cursor.execute("/*!50003 SET character_set_client  = @saved_cs_client */ ;")

        self.cursor.execute("/*!50003 SET character_set_results = @saved_cs_results */ ;")

        self.cursor.execute("/*!50003 SET collation_connection  = @saved_col_connection */ ;")


        #-- Table structure for table `box`
        print('Processing File From: Table structure for table `box`')


        self.cursor.execute("DROP TABLE IF EXISTS `box`;")

        self.cursor.execute("/*!40101 SET @saved_cs_client     = @@character_set_client */;")

        self.cursor.execute("/*!40101 SET character_set_client = utf8 */;")

        self.cursor.execute("""CREATE TABLE `box` (
          `id` bigint(20) unsigned NOT NULL,
          `mode` tinyint(1) DEFAULT '0',
          `open` tinyint(1) DEFAULT '0',
          `latitude` double(10,8) DEFAULT NULL,
          `longitude` double(11,8) DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        """)

        self.cursor.execute("/*!40101 SET character_set_client = @saved_cs_client */;")


        #-- Dumping data for table `box`
        print('Processing File From: Dumping data for table `box`')


        self.cursor.execute("LOCK TABLES `box` WRITE;")

        self.cursor.execute("/*!40000 ALTER TABLE `box` DISABLE KEYS */;")

        self.cursor.execute("INSERT INTO `box` VALUES (1,0,0,NULL,NULL);")

        self.cursor.execute("/*!40000 ALTER TABLE `box` ENABLE KEYS */;")

        self.cursor.execute("UNLOCK TABLES;")


        #-- Table structure for table `drug`
        print('Processing File From: Table structure for table `drug`')


        self.cursor.execute("DROP TABLE IF EXISTS `drug`;")

        self.cursor.execute("/*!40101 SET @saved_cs_client     = @@character_set_client */;")

        self.cursor.execute("/*!40101 SET character_set_client = utf8 */;")

        self.cursor.execute("""CREATE TABLE `drug` (
          `brand_name` varchar(80) NOT NULL,
          `gen_name` varchar(100) NOT NULL,
          `schedule` tinyint(1) DEFAULT NULL,
          `route` varchar(50) NOT NULL,
          `dosage` varchar(24) NOT NULL,
          `ndc` bigint(10) unsigned NOT NULL,
          `max_count` tinyint(3) unsigned NOT NULL DEFAULT '1',
          `weight` float(8,5) unsigned NOT NULL DEFAULT '0.50000',
          PRIMARY KEY (`ndc`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        """)

        self.cursor.execute("/*!40101 SET character_set_client = @saved_cs_client */;")


        #-- Dumping data for table `drug`
        print('Processing File From: Dumping data for table `drug`')


        self.cursor.execute("LOCK TABLES `drug` WRITE;")

        self.cursor.execute("/*!40000 ALTER TABLE `drug` DISABLE KEYS */;")

        #pywrapsql.py note: Add subroutine to process INSERT lines. 
        #I need to build ,CONCAT('20 [USP',0x27,'U]/mL - 4U'), from ,'20 [USP'U]/mL - 4U',
        self.cursor.execute("INSERT INTO `drug` VALUES ('Albuterol','Albuterol',NULL,'ORAL INHALATION','90 ug/1',2169519801,1,0.50000),('Diphenhydramine','Diphenhydramine',NULL,'INTRAVENOUS','50 mg/mL',5212524641,1,0.50000),('Bacteriastatic Sodium Chloride','Bacteriastatic Sodium Chloride',NULL,'INTRAVENOUS','25 mg/1 - 50 mg/2',5258496604,1,0.50000),('Epinephrine','Epinephrine',NULL,'INTRAVENOUS','1 mg/mL',5428810310,1,0.50000),('Methylprednisolone Sodium Succinate','Methylprednisolone Sodium Succinate',NULL,'INJECTION POWDER FOR SOLUTION','125 mg/2mL',5515495575,1,0.50000),('Sterile Water','Sterile Water',NULL,'INTRAMUSCULAR/INTRAVENOUS/SUBCUTANEOUS','',6332318500,1,0.50000),('Vasopressin','Vasopressin',NULL,'INTRAMUSCULAR/SUBCUTANEOUS',CONCAT('20 [USP',0x27,'U]/mL - 4U'),6332330201,1,0.50000),('Ranitidine Hydrochloride','Ranitidine Hydrochloride',NULL,'INTRAMUSCULAR/INTRAVENOUS','25 mg/mL - 50 mg',6584176302,1,0.50000);")

        self.cursor.execute("/*!40000 ALTER TABLE `drug` ENABLE KEYS */;")

        self.cursor.execute("UNLOCK TABLES;")


        #-- Table structure for table `equipt`
        print('Processing File From: Table structure for table `equipt`')


        self.cursor.execute("DROP TABLE IF EXISTS `equipt`;")

        self.cursor.execute("/*!40101 SET @saved_cs_client     = @@character_set_client */;")

        self.cursor.execute("/*!40101 SET character_set_client = utf8 */;")

        self.cursor.execute("""CREATE TABLE `equipt` (
          `upn` varchar(20) NOT NULL,
          `name` varchar(80) NOT NULL,
          `max_count` tinyint(3) unsigned NOT NULL DEFAULT '1',
          `weight` float(8,5) unsigned NOT NULL DEFAULT '0.50000',
          `descript` varchar(100) DEFAULT NULL,
          PRIMARY KEY (`upn`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        """)

        self.cursor.execute("/*!40101 SET character_set_client = @saved_cs_client */;")


        #-- Dumping data for table `equipt`
        print('Processing File From: Dumping data for table `equipt`')


        self.cursor.execute("LOCK TABLES `equipt` WRITE;")

        self.cursor.execute("/*!40000 ALTER TABLE `equipt` DISABLE KEYS */;")

        self.cursor.execute("INSERT INTO `equipt` VALUES ('testupn','Syringe Kit',1,0.50000,'1x 3ml Syringe, 2x Needle, 3x Alcohol Wipes');")

        self.cursor.execute("/*!40000 ALTER TABLE `equipt` ENABLE KEYS */;")

        self.cursor.execute("UNLOCK TABLES;")


        #-- Table structure for table `kit`
        print('Processing File From: Table structure for table `kit`')


        self.cursor.execute("DROP TABLE IF EXISTS `kit`;")

        self.cursor.execute("/*!40101 SET @saved_cs_client     = @@character_set_client */;")

        self.cursor.execute("/*!40101 SET character_set_client = utf8 */;")

        self.cursor.execute("""CREATE TABLE `kit` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(80) NOT NULL,
          `drug_ndc` bigint(10) unsigned DEFAULT NULL,
          `equipt_upn` varchar(20) DEFAULT NULL,
          `score` float(5,2) NOT NULL DEFAULT '50.00',
          PRIMARY KEY (`id`),
          KEY `drug_ndc` (`drug_ndc`),
          KEY `equipt_upn` (`equipt_upn`),
          CONSTRAINT `kit_ibfk_1` FOREIGN KEY (`drug_ndc`) REFERENCES `drug` (`ndc`),
          CONSTRAINT `kit_ibfk_2` FOREIGN KEY (`equipt_upn`) REFERENCES `equipt` (`upn`)
        ) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
        """)

        self.cursor.execute("/*!40101 SET character_set_client = @saved_cs_client */;")


        #-- Dumping data for table `kit`
        print('Processing File From: Dumping data for table `kit`')


        self.cursor.execute("LOCK TABLES `kit` WRITE;")

        self.cursor.execute("/*!40000 ALTER TABLE `kit` DISABLE KEYS */;")

        self.cursor.execute("INSERT INTO `kit` VALUES (3,'ANAPHYLAXIS',NULL,'testupn',50.00),(5,'ANAPHYLAXIS',2169519801,NULL,50.00),(6,'ANAPHYLAXIS',5258496604,NULL,50.00),(7,'ANAPHYLAXIS',5212524641,NULL,50.00),(8,'ANAPHYLAXIS',5515495575,NULL,50.00),(9,'ANAPHYLAXIS',6332318500,NULL,50.00),(10,'ANAPHYLAXIS',6332330201,NULL,50.00),(11,'ANAPHYLAXIS',6584176302,NULL,50.00),(19,'ANAPHYLAXIS',5428810310,NULL,50.00);")

        self.cursor.execute("/*!40000 ALTER TABLE `kit` ENABLE KEYS */;")

        self.cursor.execute("UNLOCK TABLES;")

        self.cursor.execute("/*!50003 SET @saved_cs_client      = @@character_set_client */ ;")

        self.cursor.execute("/*!50003 SET @saved_cs_results     = @@character_set_results */ ;")

        self.cursor.execute("/*!50003 SET @saved_col_connection = @@collation_connection */ ;")

        self.cursor.execute("/*!50003 SET character_set_client  = cp850 */ ;")

        self.cursor.execute("/*!50003 SET character_set_results = cp850 */ ;")

        self.cursor.execute("/*!50003 SET collation_connection  = cp850_general_ci */ ;")

        self.cursor.execute("/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;")

        self.cursor.execute("/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;")

        self.cursor.execute("""CREATE TRIGGER InsertKitEitherDrugEquipt BEFORE INSERT ON kit
        FOR EACH ROW
        BEGIN
        IF NEW.drug_ndc IS NOT NULL AND NEW.equipt_upn IS NOT NULL THEN
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT="CANNOT INSERT BOTH DRUG AND EQUIPTMENT IN SAME BIN";
        ELSEIF NEW.drug_ndc IS NULL AND NEW.equipt_upn IS NULL THEN 
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'INSERT IN KIT REQUIRES EITHER A drug_ndc OR A equipt_upn'; 
        END IF; 
        END;
        """)

        self.cursor.execute("/*!50003 SET sql_mode              = @saved_sql_mode */ ;")

        self.cursor.execute("/*!50003 SET character_set_client  = @saved_cs_client */ ;")

        self.cursor.execute("/*!50003 SET character_set_results = @saved_cs_results */ ;")

        self.cursor.execute("/*!50003 SET collation_connection  = @saved_col_connection */ ;")

        self.cursor.execute("/*!50003 SET @saved_cs_client      = @@character_set_client */ ;")

        self.cursor.execute("/*!50003 SET @saved_cs_results     = @@character_set_results */ ;")

        self.cursor.execute("/*!50003 SET @saved_col_connection = @@collation_connection */ ;")

        self.cursor.execute("/*!50003 SET character_set_client  = cp850 */ ;")

        self.cursor.execute("/*!50003 SET character_set_results = cp850 */ ;")

        self.cursor.execute("/*!50003 SET collation_connection  = cp850_general_ci */ ;")

        self.cursor.execute("/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;")

        self.cursor.execute("/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;")

        self.cursor.execute("""CREATE TRIGGER UpdateKitEitherDrugEquipt BEFORE UPDATE ON kit
        FOR EACH ROW
        BEGIN
        IF NEW.drug_ndc IS NOT NULL AND NEW.equipt_upn IS NOT NULL THEN
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT="CANNOT UPDATE TO BOTH DRUG AND EQUIPTMENT IN SAME BIN";
        ELSEIF NEW.drug_ndc IS NULL AND NEW.equipt_upn IS NULL THEN 
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'UPDATE KIT REQUIRES EITHER A drug_ndc OR A equipt_upn'; 
        END IF; 
        END;
        """)

        self.cursor.execute("/*!50003 SET sql_mode              = @saved_sql_mode */ ;")

        self.cursor.execute("/*!50003 SET character_set_client  = @saved_cs_client */ ;")

        self.cursor.execute("/*!50003 SET character_set_results = @saved_cs_results */ ;")

        self.cursor.execute("/*!50003 SET collation_connection  = @saved_col_connection */ ;")


        #-- Table structure for table `logbox`
        print('Processing File From: Table structure for table `logbox`')


        self.cursor.execute("DROP TABLE IF EXISTS `logbox`;")

        self.cursor.execute("/*!40101 SET @saved_cs_client     = @@character_set_client */;")

        self.cursor.execute("/*!40101 SET character_set_client = utf8 */;")

        self.cursor.execute("""CREATE TABLE `logbox` (
          `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
          `tm` datetime NOT NULL,
          `latitude` double(10,8) NOT NULL,
          `longitude` double(11,8) NOT NULL,
          `drug_ndc` bigint(10) unsigned DEFAULT NULL,
          `box_id` bigint(20) unsigned NOT NULL,
          `event` varchar(50) NOT NULL,
          `rfid_id` int(10) unsigned DEFAULT NULL,
          `mode` tinyint(1) DEFAULT '1',
          `equipt_upn` varchar(20) DEFAULT NULL,
          `code` tinyint(1) NOT NULL DEFAULT '0',
          `altitude` double(10,5) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `fk_log_drug_ndc` (`drug_ndc`),
          KEY `fk_log_box_id` (`box_id`),
          KEY `fk_log_rfid_id` (`rfid_id`),
          KEY `fk_logbox_equipt_upn` (`equipt_upn`),
          CONSTRAINT `fk_log_box_id` FOREIGN KEY (`box_id`) REFERENCES `box` (`id`),
          CONSTRAINT `fk_log_drug_ndc` FOREIGN KEY (`drug_ndc`) REFERENCES `drug` (`ndc`),
          CONSTRAINT `fk_log_rfid_id` FOREIGN KEY (`rfid_id`) REFERENCES `rfid` (`id`),
          CONSTRAINT `fk_logbox_equipt_upn` FOREIGN KEY (`equipt_upn`) REFERENCES `equipt` (`upn`)
        ) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=latin1;
        """)

        self.cursor.execute("/*!40101 SET character_set_client = @saved_cs_client */;")


        #-- Table structure for table `other`
        print('Processing File From: Table structure for table `other`')


        self.cursor.execute("DROP TABLE IF EXISTS `other`;")

        self.cursor.execute("/*!40101 SET @saved_cs_client     = @@character_set_client */;")

        self.cursor.execute("/*!40101 SET character_set_client = utf8 */;")

        self.cursor.execute("""CREATE TABLE `other` (
          `rfid_id` int(10) unsigned NOT NULL,
          `data` varchar(290) NOT NULL,
          UNIQUE KEY `data` (`data`),
          UNIQUE KEY `data_2` (`data`),
          KEY `rfid_id` (`rfid_id`),
          CONSTRAINT `other_ibfk_1` FOREIGN KEY (`rfid_id`) REFERENCES `rfid` (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        """)

        self.cursor.execute("/*!40101 SET character_set_client = @saved_cs_client */;")


        #-- Dumping data for table `other`
        print('Processing File From: Dumping data for table `other`')


        self.cursor.execute("LOCK TABLES `other` WRITE;")

        self.cursor.execute("/*!40000 ALTER TABLE `other` DISABLE KEYS */;")

        self.cursor.execute("INSERT INTO `other` VALUES (3,'04a8008885fd954676422806f5efcc4f881e026c46058b5e7815c475f7dc2611a1ac32d1b9c84d4cfb6cbb4ec271376585791eba885c62b1020239da0667c14e:db61b6f16cbd4d4bb6d6633b121bf7d9:b93c3450215912c928a4209f73151bd798f3f648dfe145eb94bc8e1bc8b9a789d461d03ed9737000074bb590a8af196e8d65aa4ce952ec884bb7e9181b6469eb');")

        self.cursor.execute("/*!40000 ALTER TABLE `other` ENABLE KEYS */;")

        self.cursor.execute("UNLOCK TABLES;")


        #-- Table structure for table `rfid`
        print('Processing File From: Table structure for table `rfid`')


        self.cursor.execute("DROP TABLE IF EXISTS `rfid`;")

        self.cursor.execute("/*!40101 SET @saved_cs_client     = @@character_set_client */;")

        self.cursor.execute("/*!40101 SET character_set_client = utf8 */;")

        self.cursor.execute("""CREATE TABLE `rfid` (
          `data` varchar(161) NOT NULL,
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `worker_id` bigint(20) unsigned DEFAULT NULL,
          `auth` tinyint(1) NOT NULL DEFAULT '-1',
          PRIMARY KEY (`id`),
          UNIQUE KEY `data` (`data`),
          KEY `fk_rfid_worker_id` (`worker_id`),
          CONSTRAINT `fk_rfid_worker_id` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
        """)

        self.cursor.execute("/*!40101 SET character_set_client = @saved_cs_client */;")


        #-- Dumping data for table `rfid`
        print('Processing File From: Dumping data for table `rfid`')


        self.cursor.execute("LOCK TABLES `rfid` WRITE;")

        self.cursor.execute("/*!40000 ALTER TABLE `rfid` DISABLE KEYS */;")

        self.cursor.execute("INSERT INTO `rfid` VALUES ('736047c478a399f3c033c20a8ef285214e86a8b7d05f823076c8f0e15adad32e9704f8c79ea3f9ca9411ff372b9bcb1bd0d0b0ff42c0e13dd00a557158e4124a:327b71d1b8424f158559c5ec2fece5da',1,1,0),('c9031f2675a5031ee6f4e1cdd8172d096da1d8c4370e8632e25d1ff6da3194909c9a5a8d9fdc20d8ccfcc0fd31622d0de1caacce86879a688830acc48460e3f7:352974e6a42e4613a73c4d969e59fcaf',3,2,1),('adba8969e491492ff54e07fb6ce1805e7de98e03f4ad8a21887ae6a3aa7060651d2271f90f1d8aee282b0679f9fa7dd724dc842df37fe8137020966d4918bb01:c76f8fa4202a440eb1ff3a8fe9b78389',4,3,2),('b02a4bc84878f7ac81ee256c5d72ae5851a3ce2e36e9f6f3fc280e6384fcaeeaf1eb0540fe70869e2edb1bc09074950fee0fc7732e8ae99d3f66af5bd6d994b5:3ab7ad7bac034523baee3ed09b6d8c8d',5,4,-1),('daf59a70854e0503dfc11cec4a2dadff01465a41a347195a292f69714638efcea7d5a992318b0963e6d3571f17ea061a0f375b8d75fd0bee33621a52221a020f:4c0603ea36c5491698f60f5165df9888',7,5,0),('f1f2f88a87933f2e6db29c6a6805af9f1e1bb57d293dadbc224589674c8927f9f6b6bb9be67b2e93a33bb8c80644065f0cdeea1d1b656d386e48f81c87e1cf85:cf5268bb49fc40128eb4b43954ab84a2',8,6,1);")

        self.cursor.execute("/*!40000 ALTER TABLE `rfid` ENABLE KEYS */;")

        self.cursor.execute("UNLOCK TABLES;")


        #-- Table structure for table `worker`
        print('Processing File From: Table structure for table `worker`')


        self.cursor.execute("DROP TABLE IF EXISTS `worker`;")

        self.cursor.execute("/*!40101 SET @saved_cs_client     = @@character_set_client */;")

        self.cursor.execute("/*!40101 SET character_set_client = utf8 */;")

        self.cursor.execute("""CREATE TABLE `worker` (
          `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(80) NOT NULL,
          `employer_id` bigint(20) unsigned NOT NULL,
          `employee_id` varchar(50) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
        """)

        self.cursor.execute("/*!40101 SET character_set_client = @saved_cs_client */;")


        #-- Dumping data for table `worker`
        print('Processing File From: Dumping data for table `worker`')


        self.cursor.execute("LOCK TABLES `worker` WRITE;")

        self.cursor.execute("/*!40000 ALTER TABLE `worker` DISABLE KEYS */;")

        self.cursor.execute("INSERT INTO `worker` VALUES (1,'MEDTECH TEST',1,'7678689-45'),(2,'SUPERVISOR TEST',2,'igyg-5656'),(3,'STOCKER TEST',1,'78565-886-5656'),(4,'INACTIVE TEST',4,'8b689-5656'),(5,'STOCKER TWO',1,'79288'),(6,'MEDTECH TWO',3,'88318-8d455');")

        self.cursor.execute("/*!40000 ALTER TABLE `worker` ENABLE KEYS */;")

        self.cursor.execute("UNLOCK TABLES;")

        self.cursor.execute("/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;")


        self.cursor.execute("/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;")

        self.cursor.execute("/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;")

        self.cursor.execute("/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;")

        self.cursor.execute("/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;")

        self.cursor.execute("/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;")

        self.cursor.execute("/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;")

        self.cursor.execute("/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;")

        #-- Dump completed on 2015-06-04 21:33:42
        print('Processing File From: Dump completed on 2015-06-04 21:33:42')

        self.complete()

        
    def complete(self):
        print("Processing File Complete\nDatabase: `medbox` Created")
        self.cnx.commit()
        self.cursor.close()
        self.cnx.close()

