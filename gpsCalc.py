import sys
import serial
import math

class GPS():
    def __init__(self):
        self.firstLat=0.0
        self.firstLong=0.0
        self.latDeg=0.0
        self.longDeg=0.0
        self.lastLat=0.0
        self.lastLong=0.0
        self.lastAlt=None

    def getNMEA(self):
        rawNMEA=""
        ser=serial.Serial()
        ser.baudrate=4800
        ser.port='/dev/ttyUSB0'
        ser.open()
        if not ser.isOpen():
            print("can't open serial port " + str(ser.port))
            raise SystemExit
        while True:
            try:
                line=ser.readline().decode("utf-8")[:-1]
                rawNMEA+=line
                if line[0:6]=='$GPGGA':
                    return str(rawNMEA)
            except:
                print(ser.readline())

    def meterToFt(self, met):
        return met*3.2808399
        
    def degToRad(self, deg):
        """convert decimal degree to radians"""
        return deg*(math.pi/180)

    def toLatDegrees(self, latGPS):
        """Convert GPS concatination of degrees and minutes into lat degrees.
        on latGPS 4003.8774 is 40 deg 03.8774' """
        strLat=str(latGPS)
        return float(strLat[0:2])+(float(strLat[2:])/60.0)

    def toLongDegrees(self, longGPS):
        """Convert GPS concatination of degrees and minutes into long degrees.
        on latGPS 4003.8774 is 40 deg 03.8774' """
        strLong=str(longGPS)
        return float(strLong[0:3])+(float(strLong[3:])/60.0)

    def getDistance(self, lat1,lon1,lat2,lon2):
        """arg:      2 sets of lat & long in decimal degrees
        return:      distance in miles between sets of lat & long
        Limitations: distance 'as the crow flies'.
                     Assuming at mean earth radius on a sperical earth.
                     No adjustment for altitude differneces in program yet."""    
        rlat1=self.degToRad(lat1)
        rlon1=self.degToRad(lon1)
        rlat2=self.degToRad(lat2)
        rlon2=self.degToRad(lon2)
        #haversine formula
        deltaLon=rlon2-rlon1
        deltaLat=rlat2-rlat1
        h=(math.sin(deltaLat/2)**2)+(math.cos(rlat1)*math.cos(rlat2)*math.sin(deltaLon/2)**2)
        meanEarthRadius=6371
        kmInMile=1.609344
        distance=(2*meanEarthRadius*math.asin(math.sqrt(h)))/kmInMile
        return distance



    def parseRawNMEA(self, rawStr):
        """Purpose: test other functions and play with data.
sample sentence with lat & long:
$GPGGA,175101.738,4003.8774,N,10512.6067,W,1,07,1.3,1526.5,M,-13.9,M,,0000*5C
Sentence        Sample Data     Translation
Identifier	$GPGGA	        Global Positioning System Fix Data
Time	        170834	        17:08:34 Z
Latitude	4124.8963, N	41d 24.8963' N or 41d 24' 54" N
Longitude	08151.6838, W	81d 51.6838' W or 81d 51' 41" W
Fix Quality:
- 0 = Invalid
- 1 = GPS fix
- 2 = DGPS fix	1	        Data is from a GPS fix
Number of Satellites	05	5 Satellites are in view
Horizontal Dilution of Precision (HDOP)	1.5	Relative accuracy of horizontal position
Altitude	280.2, M	280.2 meters above mean sea level
Height of geoid above WGS84 ellipsoid	-34.0, M	-34.0 meters
Time since last DGPS update	blank	No last update
DGPS reference station id	blank	No station id
Checksum	*75	        Used by program to check for transmission errors
example from: http://aprs.gids.nl/nmea/
"""
        raw=rawStr.split('\n')
        i=0
        for line in raw:
            if line[0:6]=='$GPGGA':
                i+=1
                data=line.split(',')
                try:
                    fix=int(data[6])
                except:
                    fix=0
                if fix >0:
                    try:
                        self.latDeg=self.toLatDegrees(float(data[2]))
                    except:
                        continue
                    if data[3]=="S" or data[3]=="N":
                        if data[3]=="S":
                            self.latDeg*=(-1)
                    else:
                        continue
                    try:
                        self.longDeg=self.toLongDegrees(float(data[4]))
                    except:
                        continue
                    if data[5]=="W" or data[5]=="E":
                        if data[5]=="W":
                            self.longDeg*=(-1)
                    else:
                        continue
                    print("Point "+str(((i+1)%2)+1)+")"+str(self.latDeg)+" "+str(self.longDeg))
                    if i>0 and i%2==0:
                        distMiles=self.getDistance(self.lastLat, self.lastLong, self.latDeg, self.longDeg)
                        print("dist miles=",str(distMiles), ": dist feet=", str(distMiles*5280))
                    else:
                        self.lastLat=self.latDeg
                        self.lastLong=self.longDeg
                        try:
                            self.lastAlt=self.meterToFt(data[9])
                        except:
                            self.lastAlt=None

    def getLastLatLongAlt(self, rawStr):
        """arg: raw NMEA data string
        return: last Lat and long from data in decimal degrees."""
        raw=rawStr.split('\n')[::-1] if sys.platform[0:3]=='win' else rawStr.split('\r')[::-1]
        for line in raw:
            if line[0:6]=='$GPGGA':
                data=line.split(',')
                try:
                    fix=int(data[6])
                except:
                    fix=0
                if fix >0:
                    if len(data)>5 and data[2] != 0.0 and data[4] !=0.0:
                        #print(line)
                        try:
                            self.latDeg=self.toLatDegrees(float(data[2]))
                        except:
                            continue
                        if data[3]=="S" or data[3]=="N":
                            if data[3]=="S":
                                self.latDeg*=(-1)
                        else:
                            continue
                        try:
                            self.longDeg=self.toLongDegrees(float(data[4]))
                        except:
                            continue
                        if data[5]=="W" or data[5]=="E":
                            if data[5]=="W":
                                self.longDeg*=(-1)
                        else:
                            continue
                        try:
                            self.lastAlt=self.meterToFt(float(data[9]))
                        except:
                            self.lastAlt=None
                        if self.latDeg and self.longDeg:
                            return self.latDeg, self.longDeg, self.lastAlt      
            




"""try:
    gps=GPS() 
    lastLat, lastLong = gps.getLastLatLongAlt(gps.getNMEA())
    rrccLat=39.720083
    rrccLong=-105.149545
    rrccToLastLoc=gps.getDistance(lastLat, lastLong, rrccLat, rrccLong)
    print("from "+str(lastLat)+" "+str(lastLong)+" to RRCC = "+str(rrccToLastLoc)+" as the crow flies.")
    #matches http://www.nhc.noaa.gov/gccalc.shtml results,
except KeyboardInterrupt:
    sys.exit("goodbye")"""
