# README #

This is a work in progress to prototype a medical dispensation device that authenticates on RFID.  

### RFID Med ###

* Version 0.02

### Equipment we're using ###

* Raspberry Pi 2, Raspbian, 8GB 
* RFID Reader ID-20LA (125 kHz)
* SparkFun RFID Reader Breakout
* RFID Tags 125kHz
* Potentiometers - to sense if a bin is open or closed
* A tilt sensor - to see if the box is open or closed
* RGB and Gesture Sensor
* 5" Touchscreen monitor for the user interface.
* GlobalStat BU-353 USB GPS Receiver

### Stuff we're making ###

* We're working on a prototype of a portable medicine dispenser that automatically tracks medicine.  It's being targeted for disaster relief operations.  

### More about this project ###

We're participating in the National Science Foundation Community College Innovation Challenge as a team from Red Rocks Community College.  The team consists of students Scotty Hall, Nathan Tiedt, Kaia Chapman  and Keya Lea Horiuchi.  Our faculty advisor is Helena Martellaro and our community partner is Applied Trust.

### Who do I talk to? ###

* Keya - keyalea@gmail.com
